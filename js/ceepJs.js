function responsiveNav() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}

function loadHome() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          document.getElementById("bg").innerHTML =
              this.responseText;
      }
  };
  xhttp.open("GET", "index.html", true);
  xhttp.send();
}

function loadAbout() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          document.getElementById("bg").innerHTML =
              this.responseText;
      }
  };
  xhttp.open("GET", "about.html", true);
  xhttp.send();
}

function loadFaqs() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          document.getElementById("bg").innerHTML =
              this.responseText;
      }
  };
  xhttp.open("GET", "faq.html", true);
  xhttp.send();
}

function loadPurpose() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          document.getElementById("bg").innerHTML =
              this.responseText;
      }
  };
  xhttp.open("GET", "purposes.html", true);
  xhttp.send();
}

function loadContact() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          document.getElementById("bg").innerHTML =
              this.responseText;
      }
  };
  xhttp.open("GET", "contact.html", true);
  xhttp.send();
}

function loadPurchase() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          document.getElementById("bg").innerHTML =
              this.responseText;
      }
  };
  xhttp.open("GET", "purchase.html", true);
  xhttp.send();
}

function loadProjects() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          document.getElementById("bg").innerHTML =
              this.responseText;
      }
  };
  xhttp.open("GET", "learningprojects.html", true);
  xhttp.send();
}

function loadSources() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          document.getElementById("bg").innerHTML =
              this.responseText;
      }
  };
  xhttp.open("GET", "sources.html", true);
  xhttp.send();
}

function loadGuide() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          document.getElementById("bg").innerHTML =
              this.responseText;
      }
  };
  xhttp.open("GET", "curriculumguide.html", true);
  xhttp.send();
}